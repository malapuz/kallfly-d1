<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\User;
use Storage;

class UserTransformer extends TransformerAbstract
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'username'      => $user->user,
            'full_name'     => $user->full_name,
            'user_group'    => $user->user_group,
        ];
    }

}