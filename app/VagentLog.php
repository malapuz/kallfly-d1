<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VagentLogs extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vicidial_agent_log';

    /**
     * The table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'agent_log_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

}
