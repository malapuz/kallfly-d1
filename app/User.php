<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    // use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vicidial_users';

    /**
     * The table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user', 'pass', 'full_name', 'user_level', 'user_group', 'phone_login', 'phone_pass'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin() {
        return $this->user_level == \Config::get('kallfly.type.admin');
    }

    public function getType() {
        return array_search( $this->user_level, \Config::get('kallfly.type') );
    }

    // public function getAuthPassword()
    // {
    //     return $this->pass;
    // }
}
