<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function($api) {
	
	$api->post('user/authenticate', 'App\Http\Api\Controllers\Auth\AuthController@authenticateAny');
	$api->post('admin/authenticate', 'App\Http\Api\Controllers\Auth\AuthController@authenticateAdmin');

});

$api->version('v1', ['middleware' => 'api.auth'], function ($api) {

	$api->get('token', 'App\Http\Api\Controllers\Auth\AuthController@getToken');
	$api->get('user', 'App\Http\Api\Controllers\Auth\AuthController@show');

	/**
     * Admin API
     */
	$api->group(['middleware' => 'auth.admin'], function ($api) {

		$api->get('users', 'App\Http\Api\Controllers\Auth\AuthController@index');

		$api->get('admin/cdr/get-main-stats', 'App\Http\Api\Controllers\VagentLogController@getMainStats');

	});

});