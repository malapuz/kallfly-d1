<?php

namespace App\Http\Api\Controllers\Auth;

use App\User;
use App\Transformers\UserTransformer;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Api\Controllers\Controller;
use Illuminate\Http\Request;
use League\Fractal;

class AuthController extends Controller
{
    /**
     * @var Tymon\JWTAuth\Facades\JWTAuth
     */
    protected $jwt_auth;

    public function __construct(Request $request) 
    {
        $this->jwt_auth = JWTAuth::setIdentifier('user_id');
    }
    /**
     * Authenticate User
     *
     * @param Request $request
     * @return array
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('username', 'password');

        //Temporary authenticate
        $user = User::where('user', $credentials['username'])->where('pass', $credentials['password'])->first();

        if(!$user) {
            return $this->response->errorUnauthorized('Unable to sign in with those credentials.');
        }

        $token = $this->jwt_auth->fromUser($user);
        $type = $user->getType();
        return compact('token','type','user');

        // try {
        //     if(! $token = JWTAuth::attempt($credentials))
        //     {
        //         return $this->response->errorUnauthorized('Unable to sign in with those credentials.');
        //     }
        // } catch (JWTException $ex) {
        //     return $this->response->error('Something went wrong!', 500);
        // }

        // $user = JWTAuth::toUser($token);
        // $type = $user->getType();
        // return compact('token','type','user');
    }

    /**
     * Authenticate Admin
     *
     * @param Request $request
     * @return response
     */
    public function authenticateAdmin(Request $request)
    {
        $auth = $this->authenticate($request);

        if($auth['user']->isAdmin()) {
            $user = $this->transformItem($auth['user'], new UserTransformer);
            return $this->response->array(
                array_merge( array_except($auth,['user']), compact('user') )
            )->setStatusCode(200);
        } 

        return $this->response->errorUnauthorized('Unable to sign in with those credentials.');
    }

    /**
     * Authenticate Admin or Staff
     *
     * @param Request $request
     * @return response
     */
    public function authenticateAny(Request $request)
    {
        $auth = $this->authenticate($request);
        $user = $this->transformItem($auth['user'], new UserTransformer);

        return $this->response->array(
                array_merge( array_except($auth,['user']), compact('user') )
        )->setStatusCode(200);
    }

    /**
     * Get all users
     *
     * @param  void
     * @return array Users
     */
    public function index(Request $request)
    {
        $result = $this->transformPaginatedCollection(User::paginate(), new UserTransformer, 'users');
        return $this->response->array($result)->setStatusCode(200);
    }

    /**
     * Get user
     *
     * @param  void
     * @return User
     */
    public function getUser()
    {
        try {
            $user = $this->jwt_auth->parseToken()->toUser();
            
            if(!$user) {
                return $this->response->errorNotFound('User not found');
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $ex) {
            return $this->response->errorUnauthorized('Token is invalid');
        } catch ( \Tymon\JWTAuth\Exceptions\TokenExpiredException $ex) {
            return $this->response->errorUnauthorized('Token has expired');
        } catch ( \Tymon\JWTAuth\Exceptions\TokenBlackListedException $ex) {
            return $this->response->errorUnauthorized('Token is blacklisted');
        }

        return $user;
    }

    /**
     * Get user
     *
     * @param  void
     * @return User
     */
    public function show(Request $request)
    {
        $data = $request->only('type');
        $user = $this->getUser();
        $type = $user->getType();
        $user = $this->transformItem($user, new UserTransformer);

        if (!is_null($data['type']) && $data['type'] != $type ) {
            return $this->response->errorUnauthorized("Invalid Credentials");
        }

        return $this->response->array(compact('type','user'))->setStatusCode(200);
    }


    /**
     * Refereshes the token
     *
     * @param  void
     * @return JW
     */
    public function getToken()
    {
        $token = $this->jwt_auth->getToken();

        if(!$token) {
            return $this->response->errorUnauthorized("Token is invalid");
        }

        try {
            $refreshedToken = $this->jwt_auth->refresh($token);
        } catch (JWTException $ex) {
            $this->response->error('Something went wrong');
        }

        return $this->response->array(compact('refreshedToken'));
    }
}
