<?php

namespace App\Http\Api\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;

class VagentLogController extends CrudController
{
    /**
     * Get Main Stats
     *
     * @param $request
     * @return json
     */
    public function getMainStats(Request $request)
    {
        $vagent_log = [];

        $this->validate($request, [
            'date_from' => 'required|date_format:Y-m-d',
            'date_to' => 'required|date_format:Y-m-d'
        ]);

        $req = $request->only('date_from','date_to');

        $req['date_from'] .= ' 00:00:00';
        $req['date_to'] .= ' 23:59:59';

        $username = Auth::user()->user;
        $agent_group = Auth::user()->user.'_agent';
        if(Auth::user()->user_level == \Config::get('kallfly.type.admin')) {
            $list_agents = $this->getListAgents($username, 'user');
            $username = [];
            foreach($list_agents as $key => $val){
                $username[] = $val->user;
            }
        }

        $logged_in_agents = $this->getAgentsLoggedIn($agent_group, $req['date_from'], $req['date_to']);

        $vagent_log['answered_calls'] = $this->getAgentCalls($username, $req['date_from'], $req['date_to'], 'YES', 1);
        $vagent_log['no_answer_calls'] = $this->getAgentCalls($username, $req['date_from'], $req['date_to'], 'NO', 1);
        $vagent_log['logged_in_agents'] = count($logged_in_agents);
        $agent_talk_time = $this->getAgentTalkTime($username, $req['date_from'], $req['date_to']);

        $vagent_log['total_talk_time'] = convert_seconds($agent_talk_time->total_talk, "W_HOURS");

        $total_calls = $vagent_log['answered_calls'] + $vagent_log['no_answer_calls'];
        $ans_percentage = $total_calls?($vagent_log['answered_calls'] * 100/$total_calls):0;
        $vagent_log['answer_percentage']  = round($ans_percentage, 2);
        $vagent_log['no_answer_percentage'] = $total_calls?(100 - $vagent_log['answer_percentage']):0;

        return $this->response->array(compact('vagent_log'))->setStatusCode(200);
    }

    public function getListAgents($username, $select = 'user, user_id', $return_array = true) {
        $agent_group = $username.'_agent';

        return DB::table('vicidial_users')
                ->select($select)
                ->where('user_group', $agent_group)
                ->get();
    }

    public function getAgentCalls($agent_id, $date_from, $date_to, $answered = '', $count_only = 0, $limit = 0, $last_agent_log_id = 0)
    {
        $db = DB::table('vicidial_agent_log');
        # Check if multiple agents
        if(is_array($agent_id)) {
            $db->whereIn('user', $agent_id);
        } else {
            $db->where('user', $agent_id);
        }
        # Limit
        if($limit) {
            $db->limit($limit);
        }
        if($last_agent_log_id) {
            $db->where('agent_log_id', '<', $last_agent_log_id);
        }

        # Check if answered
        $answered = strtolower($answered);
        if($answered == 'yes') {
            $db->where('talk_sec', '>', 0);
        } else if($answered == 'no') {
            $db->where('talk_sec', '=', 0);
        }

        if($count_only) {
            return $db->select('*')
                    ->where('status', '!=', '')
                    ->whereBetween('event_time', [$date_from, $date_to])
                    ->orderBy('event_time','desc')
                    ->count();

        } else {
            return $db->select('u.full_name,agent_log_id,c.campaign_name,user,event_time,lead_id,vl.phone_code,vl.phone_number,talk_epoch,talk_sec,status,user_group,comments,sub_status')
                    ->where('status', '!=', '')
                    ->whereBetween('event_time', [$date_from, $date_to])
                    ->orderBy('event_time','desc')
                    ->join('vicidial_list as vl', 'lead_id = vl.lead_id')
                    ->join('vicidial_users as u', 'user = u.user')
                    ->join('vicidial_campaigns as c', 'campaign_id = c.campaign_id')
                    ->get();
        }
    }

    public function getAgentsLoggedIn($agent_group, $date_from, $date_to) {
        return DB::table('vicidial_agent_log')->selectRaw('user, count(*)')
                    ->where('status', '!=', '')
                    ->whereBetween('event_time', [$date_from, $date_to])
                    ->where('user_group', '=', $agent_group)
                    ->groupBy('user')
                    ->get();
    }

    public function getAgentTalkTime($agent_id, $date_from, $date_to) {
        $db = DB::table('vicidial_agent_log');
        # Check if multiple agents
        if(is_array($agent_id)) {
            $db->whereIn('user', $agent_id);
        } else {
            $db->where('user', $agent_id);
        }

        return $db->selectRaw('sum(talk_sec) as total_talk')
                    ->where('status', '!=', '')
                    ->whereBetween('event_time', [$date_from, $date_to])
                    ->where('talk_sec', '>', 0)
                    ->orderBy('event_time', 'desc')
                    ->first();
    }
}
