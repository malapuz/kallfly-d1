<?php

namespace App\Http\Api\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Storage;

abstract class CrudController extends Controller
{
	/**
     * The Transformer instance
     *
     * @var \League\Fractal\TransformerAbstract
     */
    protected $transformer;

    /**
     * The Request
     *
     * @var Illuminate\Http\Request
     */
    protected $request;

    /**
     * The Storage
     *
     * @var Storage
     */
    protected $storage;

    /**
     * Crud Constructor
     *
     * @param Request
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->storage = Storage::disk('public');
    }

    /**
     * Get Paginated Collection
     *
     * @param Request
     * @return paginated collection
     */
    public function index()
    {
        $collection = $this->model->paginate();

        if (!$collection->count())
        {
            return $this->response->noContent();
        }

        return $this->transformPaginatedCollection($collection, $this->transformer, str_plural($this->key));
    }

    /**
     * Add an item
     *
     * @param UpdateRequest
     * @return model item
     */
    public function store()
    {
        $data = $this->request->all();

        //file upload
        if($this->request->files_attached) {
            foreach($this->request->files_attached as $file) {
                if($file['field_key']) {
                    $this->request->merge($file);
                    $a_file = $this->uploadFile($this->request, $file['field_key']);
                    if(!empty($a_file)) {
                        $data = array_merge($data, $a_file);
                    }
                }
            }
        }

        $result = $this->model->create($data);
        if($this->request->return_created) {
            return $result;
        }

        return $this->response->created();
    }

    /**
     * Show specific item
     *
     * @param Request
     * @return document
     */
    public function show($id)
    {
        $item = $this->model->findOrFail($id);
        return $this->transformItem($item, $this->transformer, $this->key);
    }

    /**
     * Update specific item
     *
     * @param UpdateRequest
     * @return model item
     */
    public function update($id)
    {
        $item = $this->model->findOrFail($id);
        $data = $this->request->all();

        //file upload
        if($this->request->files_attached) {
            foreach($this->request->files_attached as $file) {
                if($file['field_key']) {
                    $this->request->merge($file);
                    $a_file = $this->uploadFile($this->request, $file['field_key']);
                    if(!empty($a_file)) {
                        $data = array_merge($data, $a_file);
                    }
                }
            }
        }

        $item->update($data);
        if($this->request->return_updated) {
            return $item;
        }

        $result = $this->transformItem($item, $this->transformer);
        return $this->response->array([$this->key => $result])->setStatusCode(200);
    }

    /**
     * Delete a specific item
     *
     * @param  int $id
     * @return Message String
     */
    public function destroy($id)
    {
        $item = $this->model->findOrFail($id);
        $item->delete();

        return $this->response->array(['message' => ucfirst($this->key).' Successfully Deleted'])->setStatusCode(200);
    }
}