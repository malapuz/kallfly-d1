import {APP_URL} from '../js/config.js'

export const API_PREFIX = `${APP_URL}/api`

export const USER_RESOURCE = `${API_PREFIX}/user`;

export const ADMIN_RESOURCE = `${API_PREFIX}/admin`;
export const ADMIN_AUTHENTICATE = `${ADMIN_RESOURCE}/authenticate`;

export const CDR_RESOURCE = `${ADMIN_RESOURCE}/cdr`;
export const VAGENT_GET_MAIN_STATS = `${CDR_RESOURCE}/get-main-stats`;