import Vue, {router} from './app.js'
import {profile} from './config.js'
import {USER_RESOURCE, ADMIN_AUTHENTICATE} from './api_routes.js'

export default {
	user: {
		authenticated: false,
        profile,
		type: null
	},
	check(type, callback) {

        if (sessionStorage.getItem('id_token') != null) {
            Vue.http.get(
                USER_RESOURCE
            ).then(response => {
                this.authenticated = true
                this.user.profile = response.data.user
                this.user.type = response.data.type

                if(typeof callback == 'function') {
                    callback()
                }

                if (
                        typeof this.user.type != 'undefined' &&
                        typeof type != 'undefined' && 
                        type != this.user.type
                    ) 
                {
                    return true
                }
            }, response => {
                this.logout()
                return false
            })
        } else {
            if (
                typeof type != 'undefined' &&
                type != 'guest'
            ) {
                return false
            }
        }
		
	},
	login(context, username, password, cb) {
        Vue.http.post(
            ADMIN_AUTHENTICATE,
            {
                username,
                password
            }
        ).then(response => {
            context.error = false
            this.setToken(response.data.token)

            this.user.authenticated = true
            this.user.profile = response.data.user
            this.user.type = response.data.type

            router.go({
                name: `${this.user.type}-dashboard`
            })
        }, response => {
            context.error_message = response.data.message
            context.error = true
        })
	},
    logout() {
        sessionStorage.removeItem('id_token')
        this.user.authenticated = false
        this.user.profile = profile
        this.user.type = null

        router.go({
            name: 'home'
        })
    },
    setToken(token) {
        sessionStorage.setItem('id_token', token)
        Vue.http.headers.common['Authorization'] = 'Bearer ' + sessionStorage.getItem('id_token')
    }
}