export const APP_URL = 'http://localhost:7000';

export var profile = {
    username: '',
    full_name: '',
    user_group: '', 
};

export var vagent_log = {
    answer_percentage: 0,
    answered_calls: 0,
    logged_in_agents: 0, 
    no_answer_calls: 0, 
    no_answer_percentage: 0, 
    total_talk_time: '00:00:00', 
};