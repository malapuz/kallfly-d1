import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import KeenUI from 'keen-ui'

import {APP_URL} from './config.js'
import App from '../components/App.vue'
import Login from '../components/Login.vue'

import Admin from '../components/admin/Admin.vue'
import AdminDashboard from '../components/admin/Dashboard.vue'
import AdminAccount from '../components/admin/Account.vue'

Vue.use(VueRouter)
Vue.use(VueResource)
Vue.use(KeenUI)

Vue.config.devtools = true
Vue.config.debug = true

Vue.http.headers.common['X-CSRF-TOKEN'] = document.getElementsByName('csrf-token')[0].getAttribute('content')
Vue.http.headers.common['Authorization'] = 'Bearer ' + sessionStorage.getItem('id_token')

Vue.http.options.root = APP_URL

export default Vue
export var router = new VueRouter({hashbang: false})

router.map({
    '/': {
        name: 'home',
        component: Login
    },
    '/admin': {
        component: Admin,
        subRoutes: {
            '/dashboard': {
                name: 'admin-dashboard',
                component: AdminDashboard
            },
            '/account': {
                name: 'admin-account',
                component: AdminAccount
            }
        }
    }

})

//404 page
router.redirect({
    '*': '/'
})

Vue.http.interceptors.push((request, next) => {
    next((response) => {
        if (response.status == 401 && response.body.message == 'Token has expired') {
            router.go({ name: 'home' })
        }
    })
})

router.start(App, '#app')