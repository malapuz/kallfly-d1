import Vue, {router} from './app.js'
import {vagent_log} from './config.js'
import {VAGENT_GET_MAIN_STATS} from './api_routes.js'

export default {
	vagent_log,
	getMainStats(context, date_from, date_to, cb) {
		Vue.http.get(
            VAGENT_GET_MAIN_STATS,
            { params: { date_from, date_to } }
        ).then(response => {

            if(typeof cb == 'function') {
                cb(response.data)
            }

        }, response => { })
	}
}